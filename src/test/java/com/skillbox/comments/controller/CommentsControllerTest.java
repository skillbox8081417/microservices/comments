package com.skillbox.comments.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.skillbox.comments.base.BaseTest;
import com.skillbox.comments.client.PostsClient;
import com.skillbox.comments.domain.Comment;
import com.skillbox.comments.dto.EventRecord;
import com.skillbox.comments.repo.CommentsRepo;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
@EnableConfigurationProperties
@ActiveProfiles("test")
class CommentsControllerTest extends BaseTest {
    @Autowired
    private WebApplicationContext webContext;
    private MockMvc mockMvc;
    @Autowired
    private PostsClient postsClient;
    @Autowired
    private CommentsRepo commentsRepo;
    private WireMockServer mockImageService;
    private Comment comment;
    private ObjectMapper objectMapper;
    private static KafkaConsumer<String, String> kafkaConsumer;

    @BeforeEach
    public void init() throws IOException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
        mockImageService = new WireMockServer(
                new WireMockConfiguration().port(9611)
        );
        mockImageService.start();
        WireMock.configureFor("localhost", 9611);
        comment = new Comment();
        comment.setComment("Comment");
        comment.setCommentId(UUID.randomUUID());
        comment.setCommentDate(LocalDateTime.now());
        comment.setPostId(123456L);
        comment.setAuthorId(1234567L);
        comment.setNumberOfLikes(1L);
        comment = commentsRepo.save(comment);
        comment = commentsRepo.findByCommentId(comment.getCommentId());
        Assertions.assertNotNull(comment);
        objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        kafkaConsumer = getKafkaConsumer();
        kafkaConsumer.subscribe(List.of("statistics"));
    }
    @AfterEach
    public void stop () {
        mockImageService.stop();
    }

    @AfterAll
    static void close() {
        // Close the consumer before shutting down Testcontainers Kafka instance
        kafkaConsumer.close();
    }

    @Test
    void isPostExists() throws JSONException {
        mockImageService.stubFor(WireMock.get(WireMock.urlMatching("/posts/isPostExists.[0-9]*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withBody("true")));
        Assert.assertEquals("true", postsClient.isPostExists(comment.getPostId()));
    }

    @Test
    void addCommentsToPost() throws Exception {
        mockImageService.stubFor(WireMock.get(WireMock.urlMatching("/posts/isPostExists.[0-9]*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withBody("true")));
        String request = objectMapper.writeValueAsString(comment);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put("/comments/posts/{id}/addComments", comment.getPostId())
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, "Bearer " + getToken())
                .content(request));
        resultActions.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.commentId", Matchers.equalTo(comment.getCommentId().toString())));
    }

    @Test
    void updateComment() throws Exception {
        comment.setComment("NewComment");
        String request = objectMapper.writeValueAsString(comment);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/comments/posts/comments/{id}", comment.getCommentId())
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, "Bearer " + getToken())
                .content(request));
        resultActions.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.comment", Matchers.equalTo("NewComment")));
    }

    @Test
    void deleteComment() throws Exception {
        String request = objectMapper.writeValueAsString(comment);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/comments/posts/comments/{id}", comment.getCommentId())
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, "Bearer " + getToken())
                .content(request));
        resultActions.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$",
                        Matchers.equalTo("Comment with id: "
                                + comment.getCommentId() +" has been deleted from the database")));
    }

    @Test
    void getCommentById() throws Exception {
        String request = objectMapper.writeValueAsString(comment);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/comments/posts/comments/{id}", comment.getCommentId())
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, "Bearer " + getToken())
                .content(request));
        resultActions.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.comment", Matchers.equalTo(comment.getComment())));
    }

    @Test
    void getAllCommentsForPost() throws Exception {
        mockImageService.stubFor(WireMock.get(WireMock.urlMatching("/posts/isPostExists.[0-9]*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withBody("true")));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/comments/posts/{id}/getAllComments",comment.getPostId())
                .header(AUTHORIZATION, "Bearer " + getToken())
                .accept(MediaType.APPLICATION_JSON));
        MvcResult result = resultActions.andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();
        String content = result.getResponse().getContentAsString();
        JSONArray commentsArray = new JSONArray(content);
        Assertions.assertNotEquals(0, commentsArray.length());
    }

    @Test
    void likeComment() throws Exception {
        String request = objectMapper.writeValueAsString(comment);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put("/comments/posts/comments/{id}/like", comment.getCommentId())
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, "Bearer " + getToken())
                .content(request));
        resultActions.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.numberOfLikes", Matchers.equalTo(2)));
        // Verify message produced onto statistics Topic
        assertDoesNotThrow(
                () -> KafkaTestUtils.getSingleRecord(kafkaConsumer, "statistics", Duration.ofSeconds(5)), "No records found for topic");
    }
}