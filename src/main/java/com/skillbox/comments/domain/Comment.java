package com.skillbox.comments.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document(collection = "comments")
@Getter
@Setter
@ToString
public class Comment {
    @Id
    private UUID commentId;
    private Long postId;
    private String comment;
    private LocalDateTime commentDate;
    private Long authorId;
    private Long numberOfLikes;
}
