package com.skillbox.comments.repo;

import com.skillbox.comments.domain.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;

public interface CommentsRepo extends MongoRepository<Comment, UUID> {
    List<Comment> findByPostId(Long postId);

    Comment findByCommentId(UUID commentId);

}
