package com.skillbox.comments.service;

import com.skillbox.comments.domain.Comment;
import com.skillbox.comments.repo.CommentsRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentsService {
    private final CommentsRepo commentsRepo;

    public Comment getCommentById (UUID commentId) {
        return commentsRepo.findByCommentId(commentId);
    }

    public List<Comment> getCommentsByPostId(Long postId) {
        return  commentsRepo.findByPostId(postId);
    }

    public Comment createComment(Comment comment) {
        return commentsRepo.save(comment);
    }

    public Comment updateComment(Comment comment) {
        return commentsRepo.save(comment);
    }

    public List<Comment> findCommentsByPostId(Long postId) {
        return commentsRepo.findByPostId(postId);
    }

    public String deleteComment(UUID commentId) {
        if (!commentsRepo.existsById(commentId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        commentsRepo.deleteById(commentId);
        return String.format("Comment with id: %s has been deleted from the database", commentId);
    }

    public Comment likeComment(Comment comment) {
        comment.setNumberOfLikes(comment.getNumberOfLikes() + 1);
        return commentsRepo.save(comment);
    }
}
