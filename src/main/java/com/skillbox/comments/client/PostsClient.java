package com.skillbox.comments.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

@FeignClient (name = "image-api", url = "${app.services.image-api.url}")
public interface PostsClient {

    @GetMapping(value = "/posts/isPostExists/{id}")
    String isPostExists(@PathVariable Long id) throws ResponseStatusException;
}
