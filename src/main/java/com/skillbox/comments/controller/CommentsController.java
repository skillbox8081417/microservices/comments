package com.skillbox.comments.controller;

import com.skillbox.comments.client.PostsClient;
import com.skillbox.comments.domain.Comment;
import com.skillbox.comments.dto.EventRecord;
import com.skillbox.comments.producer.EventKafkaProducer;
import com.skillbox.comments.service.CommentsService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
@RequestMapping("/comments")
@RequiredArgsConstructor
public class CommentsController {
    private final CommentsService commentsService;
    private final EventKafkaProducer eventKafkaProducer;
    private final PostsClient postsClient;


    @Operation(summary = "Add comment to the post")
    @PutMapping("posts/{id}/addComments")
    public Comment addCommentsToPost(@PathVariable Long id, @RequestBody Comment comment) {
        if (!"true".equals(postsClient.isPostExists(id))) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        comment.setPostId(id);
        return commentsService.createComment(comment);
    }

    @Operation(summary = "Update comment")
    @PostMapping("posts/comments/{id}")
    public Comment updateComment(@PathVariable UUID id, @RequestBody Comment comment) {
        return commentsService.updateComment(comment);
    }

    @Operation(summary = "delete comment by id")
    @DeleteMapping("posts/comments/{id}")
    public String deleteComment(@PathVariable UUID id) {
        return commentsService.deleteComment(id);
    }

    @Operation(summary = "Find comment by id")
    @GetMapping("posts/comments/{id}")
    public Comment getCommentById(@PathVariable UUID id) {
        return commentsService.getCommentById(id);
    }

    @Operation(summary = "Find all comments by post id")
    @GetMapping("posts/{id}/getAllComments")
    public List<Comment> getAllCommentsForPost(@PathVariable Long id) {
        if (!"true".equals(postsClient.isPostExists(id))) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return commentsService.findCommentsByPostId(id);
    }

    @Operation(summary = "Set like for the comment")
    @PutMapping("/posts/comments/{id}/like")
    public Comment likeComment(@PathVariable UUID id) throws ExecutionException, InterruptedException {
        Comment comment = commentsService.getCommentById(id);
        if (comment == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        EventRecord event = EventRecord.builder()
                .uuid(UUID.randomUUID())
                .eventInitiator(comment.getAuthorId().toString())
                .objectId(comment.getCommentId().toString())
                .objectType("Comment")
                .dateTime(LocalDateTime.now())
                .build();
        eventKafkaProducer.sendMessage(event);
        return commentsService.likeComment(comment);
    }


}
